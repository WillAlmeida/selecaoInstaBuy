## Seleção Dev InstaBuy

# Instalação

Instale o pip para conseguir instalar os pacotes de python exigidos

```
$ [sudo] apt-get install python-pip
```

Para instalação rápida dos pacotes exigidos execute essa linha

```
$ [sudo] pip install -r requirements.txt
```

Instale o `virtualenv` para ter um ambiente com as versões isoladas dos frameworks/pacotes

```
$ [sudo] pip install virtualenv
```

Crie um ambiente no `virtualenv` com o nome de `env`

```
$ virtualenv -p python3.5 env
$ source env/bin/activate
```

Para sair da virtualenv

```
$ deactivate
```

# Configurando o Database

Se você nunca usou MySQL com o Django, execute as seguintes linhas para instalar os pacotes necessários
```
$ sudo apt-get update
$ sudo apt-get install python-dev mysql-server libmysqlclient-dev
```
Após configurar sua conta administrativa MySQL, rode esse comando para o diretório do banco de dados
```
$ sudo mysql_secure_installation
```
Após realizar os passos anteriores, agora é necessário criar o banco de dados, é sugerido que se preencha os dados de acordo com o arquvio settings.py
```
CREATE DATABASE selecaodb CHARACTER SET UTF8;

CREATE USER db_user@localhost IDENTIFIED BY 'db_password';

GRANT ALL PRIVILEGES ON selecaodb.* TO db_user@localhost;

FLUSH PRIVILEGES;

exit
```

Após esse passo certifique que os campos do banco de dados criado conferem com o arquivo settings.py na seção DATABASE.

Por último, com sua virtualenv ligada, execute o comando abaixo para integrar o banco de dados com o django
```
$ pip install django mysqlclient
```
# Subindo o servidor
Antes de tudo, execute os seguintes códigos para efetuar a migração do banco de dados e a integração com o django
```
$ python manage.py makemigrations
$ python manage.py migrate
```


Para subir o servidor da API basta digitar a linha de código abaixo

```
$ python manage.py runserver
```
O servidor por padrão estará ligado na porta 8000 do localhost

Para sair do servidor use o atalho CTRL+C
