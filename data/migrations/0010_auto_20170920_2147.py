# -*- coding: utf-8 -*-
# Generated by Django 1.11.5 on 2017-09-20 21:47
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('data', '0009_auto_20170920_2129'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='person',
            name='person_address',
        ),
        migrations.AddField(
            model_name='person',
            name='person_address',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='data.Address', verbose_name='addresses'),
        ),
    ]
