from django.shortcuts import get_object_or_404, render
from rest_framework import status, viewsets
from rest_framework.decorators import api_view
from rest_framework.response import Response
from selecao.serializers import AddressSerializer, PersonSerializer
from .models import Address, Person
from django.contrib import messages
from django.http import HttpResponse
import json
from rest_framework.decorators import detail_route
# Create your views here.

class PersonViewSet(viewsets.ModelViewSet):
    queryset = Person.objects.all()
    serializer_class = PersonSerializer


class AddressViewSet(viewsets.ModelViewSet):
    queryset = Address.objects.all()
    serializer_class = AddressSerializer

    def list(self, request, *args, **kwargs):
        self.queryset = self.queryset.filter_by_request_params(request)
        return super(AddressViewSet, self).list(request, *args, **kwargs)
