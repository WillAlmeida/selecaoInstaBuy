from django.db import models
from django.core.validators import RegexValidator

# Create your models here.

#Search address by personid
class AddressManager(models.QuerySet):
    def filter_by_request_params(self, request):
        person_str = request.GET.get('person_id', None)
        if person_str:
            self = self.filter(person_id=int(person_str.strip()))
        return self

class Address(models.Model):
    zipcode = models.CharField(max_length=9)
    state = models.CharField(max_length=100)
    city = models.CharField(max_length=100)
    street = models.CharField(max_length=100)
    number = models.IntegerField()
    person_id = models.ForeignKey('Person', on_delete=models.CASCADE, related_name="addresses")
    objects = AddressManager.as_manager()


class Person(models.Model):
    name = models.CharField(max_length=100)
    age = models.IntegerField()
    cpf_regex = RegexValidator(regex=r'^\d{11}$',message="""CPF inválido, tente novamente""")
    cpf = models.BigIntegerField(validators=[cpf_regex])

    def __str__(self):
        return self.name
