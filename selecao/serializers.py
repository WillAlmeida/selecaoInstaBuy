from data.models import Address, Person
from rest_framework import serializers
import json


class AddressSerializer(serializers.ModelSerializer):
    class Meta:
        model = Address
        fields = [
            'id',
            'zipcode',
            'state',
            'city',
            'street',
            'number',
            'person_id',
        ]


class PersonSerializer(serializers.ModelSerializer):
    class Meta:
        model = Person
        fields = [
            'id',
            'name',
            'age',
            'cpf',
            'addresses',
        ]
        read_only_fields = ['addresses']
